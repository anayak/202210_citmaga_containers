{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Intro to Apptainer `app` : A use-case with FEniCS"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Share and reproduce results"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Apptainer offers a powerful feature for reproduceability in the form of container **`app`** s. \n",
    "\n",
    "**`App`** s within a container are preset scripts to reproduce results. \n",
    "\n",
    "What do I mean? \n",
    "\n",
    "Let's check with a simple demo. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's pull the latest `demo_dolfinx`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "# apptainer pull library://testgroup/random/demo_dolfinx:latest"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's check the container metadata for direction,"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "    # ===============================================\n",
      "    # Demo Container file for DISC-Reading Group talk \n",
      "    # ===============================================\n",
      "    A DOLFINX demo container to demonstrate the use-cases of apptainer for a scientific computing project.\n",
      "    #\n",
      "    Check for available apps using,\n",
      "        apptainer inspect --list-apps demo_dolfinx.sif\n",
      "    #\n",
      "    For more info on running apps use,\n",
      "        apptainer inspect --helpfile --app APPNAME demo_dolfinx.sif\n"
     ]
    }
   ],
   "source": [
    "apptainer inspect --helpfile demo_dolfinx.sif"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us check for available `app` names: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Poisson_Solver\n",
      "Wave_Equation_convergence_study\n"
     ]
    }
   ],
   "source": [
    "apptainer inspect --list-apps demo_dolfinx.sif"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### How do we run apps?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lets check the apps for more information"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "    # -----------------------------------------------------------------------\n",
      "    # Demo Plot App : Wave equation solver\n",
      "    # -----------------------------------------------------------------------\n",
      "    An app for reproducing FEM Convergence plots of the wave equation solver. \n",
      "    This app writes the plots as PNG files to current working directory.\n",
      "\n",
      "    # Source Code\n",
      "    https://gitlab.mpi-magdeburg.mpg.de/anayak/demo_wave_equation.git\n",
      "\n",
      "    # Run app using,\n",
      "    apptainer run --app Wave_Equation_convergence_study demo_dolfinx.sif\n"
     ]
    }
   ],
   "source": [
    "apptainer inspect --helpfile --app Wave_Equation_convergence_study demo_dolfinx.sif"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "apptainer run --app Wave_Equation_convergence_study demo_dolfinx.sif"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### REMARK : Visualization of results"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`app`s can also be used for interactive visualization of desired results! \n",
    "\n",
    "Let us see the other app in the demo container"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "    # -----------------------------------------------------------------------\n",
      "    # Demo Plot App : Solve Poisson problem and Visualize \n",
      "    # -----------------------------------------------------------------------\n",
      "    An app for running a Poisson solver demo and visualizes the solution.\n",
      "    This app fires a GUI frontend for interactive Visualization of a 3D plot using PyVista.\n",
      "\n",
      "    # Source Code \n",
      "    https://github.com/FEniCS/dolfinx/blob/v0.5.1/python/demo/demo_poisson.py\n",
      "\n",
      "    # Run app using,\n",
      "    apptainer run --app Poisson_Solver demo_dolfinx.sif\n"
     ]
    }
   ],
   "source": [
    "apptainer inspect --helpfile --app Poisson_Solver demo_dolfinx.sif"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "apptainer run --app Poisson_Solver demo_dolfinx.sif"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Why container `app`s over user-defined scripts?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`app`s offer a standardized way to interact with the container without requiring knowledge about the contents of the container. \n",
    "Apps are encoded into the filesystem as a modular overlay on the base container image according to the [SCIF Filesystem](https://sci-f.github.io/) which enables a pre-defined interface to connect different containers. \n",
    "\n",
    "It is important in software development to recognize not just the user-interaction with the software, but also how it falls into place with other software in a well-defined way. For example, policies to handle security concerns in the event of scripts requiring elevated privileges, apps are preferred channel to manage permissions compared to user-defined scripts and policies. Hence, clients and integrations that meet this specification to interact with a (SCIF) container can parse these directories programatically without knowing specifics of the software apps installed."
   ]
  }
 ],
 "metadata": {
  "interpreter": {
   "hash": "f92b2924b84ff19c1c3dc485f7644d4486f64738191026bf8e6de303969141b5"
  },
  "kernelspec": {
   "display_name": "Bash",
   "language": "bash",
   "name": "bash"
  },
  "language_info": {
   "codemirror_mode": "shell",
   "file_extension": ".sh",
   "mimetype": "text/x-sh",
   "name": "bash"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
