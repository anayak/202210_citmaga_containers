# Apptainer Resources

- [Singularity Docs](https://docs.sylabs.io/guides/3.0/user-guide/index.html)
- [Apptainer Docs](https://apptainer.org/docs/admin/main/index.html)
- [TACC Workshop](https://containers-at-tacc.readthedocs.io/en/latest/index.html)
- [ICL Course (Old)](https://imperialcollegelondon.github.io/2020-07-13-Containers-Online/10-singularity-containers/index.html)