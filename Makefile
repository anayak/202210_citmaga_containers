BUILDDIR=output
FIGS=$(addprefix figures/, apptainer-logo.png docker-logo.png)
DEPS=Makefile *.tex */*.tex $(FIGS)
DIRS=$(BUILDDIR) figures
PDF=$(BUILDDIR)/$(filename).pdf

filename=ppt
exlatex=pdflatex
SHELL=/bin/bash
FLAGS=-synctex=1 -interaction=nonstopmode --shell-escape -output-directory=$(BUILDDIR)

.PHONY: update
update: $(FIGS) | $(DIRS)
	${exlatex} ${FLAGS} ${filename}

.PHONY: all
all: $(PDF)

$(PDF): $(DEPS) | $(DIRS)
	${exlatex} ${FLAGS} ${filename}
	${exlatex} ${FLAGS} ${filename}

$(DIRS):
	mkdir -p $@

.PHONY: clear
clear:
	@# activate negative wildcard `!(...)` using `-O extglob`
	bash -O extglob -c "rm -rf ./$(BUILDDIR)/!(*.pdf)"

.PHONY: clean
clean:
	rm -rf $(BUILDDIR) figures

figures/apptainer-logo.png: | figures
	wget -O $@ https://apptainer.org/docs/user/main/_static/logo.png

figures/docker-logo.png: | figures
	wget -O $@ https://www.docker.com/wp-content/uploads/2022/03/Moby-logo.png
