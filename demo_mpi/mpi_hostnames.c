#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <mpi.h>

int main(int argc, char **argv){
	int pid, pnr;
	size_t hlen=127;
	char hostname[128];
	MPI_Init(&argc, &argv);
	if( gethostname(hostname, hlen)){
		fprintf(stderr, "error: gethostname\n");
		return -1;
	}
	MPI_Comm_rank(MPI_COMM_WORLD, &pid);
	MPI_Comm_size(MPI_COMM_WORLD, &pnr);
	printf("%s: process %d of %d\n", hostname, pid, pnr);
	MPI_Finalize();
	return 0;
}

