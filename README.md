##  Reproducible scientific workflow with Apptainer

## Description
Latex Source code and Demo scripts for the seminar with [CITMaGa](https://citmaga.gal).

[Download PDF (build)](https://gitlab.mpi-magdeburg.mpg.de/anayak/202210_citmaga_containers/-/jobs/artifacts/main/raw/output/ppt.pdf?job=build)

## Demo
The demos use a [`bash` kernel](https://github.com/takluyver/bash_kernel) for Jupyter Notebooks.
Install as follows:

```bash
# ensure ~/.local/bin is in the search path; run if needed:
PATH="$HOME/.local/bin:$PATH"

# install packages
pip3 install jupyter bash_kernel

# register kernel
LOCATION=$(pip3 show bash_kernel | grep Location | cut -d' ' -f2)
python3 $LOCATION/bash_kernel/install.py
```
